<?php declare(strict_types=1);

namespace drew\complex;

class ComplexNumber
{
    protected float $realPart;
    protected float $imaginaryPart;

    protected function __construct(float $realPart, float $imaginaryPart)
    {
        $this->realPart = $realPart;
        $this->imaginaryPart = $imaginaryPart;
    }

    public static function build(float $realPart, float $imaginaryPart): ComplexNumber
    {
        return new self($realPart, $imaginaryPart);
    }

    /**
     * @throws \InvalidArgumentException
     */
    public static function fromLine(string $line): ComplexNumber
    {
        $pattern = '%(?P<real>[\d\.\-]+)\s(?P<sign>[\+\-])\s(?P<imaginary>[\d\.]+)?i%';
        $rst = preg_match($pattern, $line, $m);
        if ($rst === 0) {
            throw new \InvalidArgumentException($line);
        }

        if (!isset($m['imaginary'])) {
            $m['imaginary'] = 1;
        }
        $realPart = (float)$m['real'];
        $imaginaryPart = $m['sign'] === '+' ? (float)$m['imaginary'] : (float)-$m['imaginary'];

        return ComplexNumber::build($realPart, $imaginaryPart);
    }

    public function getRealPart(): float
    {
        return $this->realPart;
    }

    public function getImaginaryPart(): float
    {
        return $this->imaginaryPart;
    }

    public function isNull(): bool
    {
        return $this->realPart == 0 && $this->imaginaryPart == 0;
    }

    public function __toString()
    {
        $sign = $this->imaginaryPart >= 0 ? '+' : '-';
        $imaginary = ($this->imaginaryPart != 1) ? abs($this->imaginaryPart) : '';

        return $this->realPart . ' ' .  $sign . ' ' . $imaginary . 'i';
    }
}
