<?php declare(strict_types=1);

namespace drew\complex;

class ComplexCalculator
{

    public static function summarize(ComplexNumber $numA, ComplexNumber $numB): ComplexNumber
    {
        $realPart = $numA->getRealPart() + $numB->getRealPart();
        $imaginaryPart = $numA->getImaginaryPart() + $numB->getImaginaryPart();

        return ComplexNumber::build($realPart, $imaginaryPart);
    }

    public static function subtract(ComplexNumber $numA, ComplexNumber $numB): ComplexNumber
    {
        $realPart = $numA->getRealPart() - $numB->getRealPart();
        $imaginaryPart = $numA->getImaginaryPart() - $numB->getImaginaryPart();

        return ComplexNumber::build($realPart, $imaginaryPart);
    }

    public static function multiply(ComplexNumber $numA, ComplexNumber $numB): ComplexNumber
    {
        $realPart = $numA->getRealPart() * $numB->getRealPart()
                - $numA->getImaginaryPart() * $numB->getImaginaryPart();

        $imaginaryPart = $numA->getRealPart() * $numB->getImaginaryPart()
                + $numA->getImaginaryPart() * $numB->getRealPart();

        return ComplexNumber::build($realPart, $imaginaryPart);
    }

    /**
     * @throws \LogicException
     */
    public static function divide(ComplexNumber $numA, ComplexNumber $numB): ComplexNumber
    {
        if ($numB->isNull()) {
            throw new \LogicException('Cannot divide by zero');
        }

        $realPartNumerator = $numA->getRealPart() * $numB->getRealPart()
                + $numA->getImaginaryPart() * $numB->getImaginaryPart();

        $imaginaryPartNumerator = $numA->getImaginaryPart() * $numB->getRealPart()
                - $numA->getRealPart() * $numB->getImaginaryPart();

        $denominator = pow($numB->getRealPart(), 2) + pow($numB->getImaginaryPart(), 2);

        $realPart = $realPartNumerator / $denominator;
        $imaginaryPart = $imaginaryPartNumerator / $denominator;

        return ComplexNumber::build($realPart, $imaginaryPart);
    }
}
