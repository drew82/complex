<?php

require_once __DIR__ . '/vendor/autoload.php';

use \drew\complex\ComplexNumber;
use \drew\complex\ComplexCalculator;

// test parser
$lines = [
    '5 + i',
    '0.33 - 5i',
    '-1.75 - 2.34i',
];
foreach ($lines as $l) {
    var_dump((string)ComplexNumber::fromLine($l) === $l);
}

// test null
var_dump(ComplexNumber::fromLine('0 + 0i')->isNull());

// test sum
var_dump((string)ComplexCalculator::summarize(
        ComplexNumber::fromLine('2 + 7i'), ComplexNumber::fromLine('5 - 3i')
        ) === '7 + 4i');

// test sub
var_dump((string)ComplexCalculator::subtract(
        ComplexNumber::fromLine('8 + 7i'), ComplexNumber::fromLine('5 - 2i')
        ) === '3 + 9i');

// test mult
var_dump((string)ComplexCalculator::multiply(
        ComplexNumber::fromLine('2 + 3i'), ComplexNumber::fromLine('3 - i')
        ) === '9 + 7i');

// test div
var_dump((string)ComplexCalculator::divide(
        ComplexNumber::fromLine('2 - i'), ComplexNumber::fromLine('1 + i')
        ) === '0.5 - 1.5i');
